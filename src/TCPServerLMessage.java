import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerLMessage extends TCPServerBuilder implements Runnable {

    @Override
    public void run() {
        try {
            setSocket();
            System.out.println("TCPServerLMessage launched ...");

            while(true) {
                s = ss.accept();
                new Thread(new ServerLMessage(s)).start();
            }
        } catch (Exception var4) {
            try {
                ss.close();
            } catch (Exception var3) {
            }

            System.out.println("Exception TCPServerLMessage");
        }

    }
}
