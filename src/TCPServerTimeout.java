import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerTimeout extends TCPServerBuilder implements Runnable {
    private ServerSocket ss;
    private Socket s;
    @Override
    public void run() {
        try {
            ss = new ServerSocket(8085); ss.setSoTimeout(5000);
            while(true) {
                s = ss.accept();
                new Thread(new ServerTimeout(s)).start();
            }
        }
        catch(IOException e)
        { System.out.println("IOException TCPServerTimeout"); }
    }

}
