import java.io.IOException;
import java.io.InputStream;

public class TCPServerMessage extends TCPServerBuilder implements Runnable {
    public void run( ) {
        try {
            setSocket();
            while(true) {
                s = ss.accept();
                in = s.getInputStream();
                msgIn = readMessage(in);
                System.out.println(msgIn);
                in.close();
                s.close();
            }
        } catch (IOException e) {
            try {
                ss.close();
            } catch (IOException e1) {
                e1.printStackTrace();
                System.out.println("Exception TCPServerMessage");
            }
            e.printStackTrace();
        }
    }
}
