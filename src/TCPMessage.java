import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

public class TCPMessage extends TCPInfo{
    protected byte[] buffer;
    private final int size = 8192;
    private long scount;
    private long n;
    private Random r = new Random();

    /** The set method for the buffer. */
    void setStreamBuffer(int size) {
        if(size>0)
            buffer = new byte[size];
        else
            buffer = new byte[size];
    }
    /** The (simple) text write method. */
    public void writeMessage(OutputStream out, String msOut) throws IOException {
        if((out!=null)&(msOut!=null)) {
            fillChar(msOut);
            out.write(buffer);
            out.flush();
            clearBuffer();
        }
    }

    private void fillChar(String msOut) {
        if(msOut!=null)
            if(msOut.length() < buffer.length)
            for(int i=0;i<msOut.length();i++)
                buffer[i] = (byte)msOut.charAt(i);
    }
    void clearBuffer() {
        for(int i=0;i<buffer.length;i++)
            buffer[i] = 0;
    }
    /** The (simple) text read method. */
    public String readMessage(InputStream in) throws IOException {
        if(in != null) {
            in.read(buffer); count = count();
            if(count>0)
                return new String(buffer,0,count);
        }
        return null;
    }
    private double mb(long scount) {
        return Math.rint((double)scount * 10000.0D / 1048576.0D) / 10000.0D;
    }
    void loopReadMessage(InputStream in) throws IOException {
        n = (long)(count = 0);
        scount = 0L;

        do {
            count = in.read(buffer);
            if (count > 0) {
                scount += (long)count;
                System.out.println("read \t" + mb(scount) + "\t Mbytes \t" + n + "\t loops");
                ++n;
            }
        } while(count > 0);
        System.out.println("\nSize of all data received :" + mb(scount) + " Mbytes");
    }
    private int count;
    protected int count() {
        for(int i=0;i<buffer.length;i++)
            if(buffer[i] == 0)
                return i;
        return buffer.length;
    }

    public int getReceiveBufferSize() {
        return buffer.length;
    }
    public int getSendBufferSize() {
        return buffer.length;
    }
    void loopWriteMessage(OutputStream out, int loop) throws IOException {
        for(int i=0;i<loop;i++) {
            fillAtRandom(buffer);
            out.write(buffer);
            out.flush();
        }
    }
    private void fillAtRandom(byte[] buffer) {
        for(int i=0; i<buffer.length; i++)
            buffer[i] = (byte)r.nextInt(256); //Fill the buffer with random integer
    }
}
