public class TCPServerFile extends TCPServerBuilder implements Runnable {
    @Override
    public void run(){
        try {
            setSocket();
            System.out.println("TCPServerFile launched ...");

            while(true) {
                s = ss.accept();
                (new Thread(new ServerFile(s))).start();
            }
        } catch (Exception var4) {
            try {
                ss.close();
            } catch (Exception var3) {
                System.out.println("Exception TCPServerFile");
            }

        }

    }
}
