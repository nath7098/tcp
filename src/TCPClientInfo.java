import java.io.IOException;

public class TCPClientInfo extends TCPClientBuilder implements Runnable {
    public void run( ) {
        try {
            setSocket();
            sInfo("The client following the socket acceptance", s);
            s.close();
            sInfo("The client closes the socket", s);
        } catch (Exception var2) {
            System.out.println("Exception TCPClientInfo");
        }

    }
}
