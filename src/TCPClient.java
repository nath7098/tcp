import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPClient extends TCPInfo implements Runnable {

    private Socket s;
    private InetSocketAddress isA;
    // the client socket
//the remote address
/** The builder. */
    TCPClient() {
        s = null;
        isA = new InetSocketAddress("localhost",8085);
    }
    /** The main method for threading. */
    public void run() {
        try {
            System.out.println("TCPClient launched ...");
            s = new Socket(isA.getHostName(), isA.getPort());
            System.out.println("Hello, the client is connected");
            s.close();
        } catch (Exception var2) {
            System.out.println("Exception TCPClient");
        }
    }
}
