import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class TCPClientBuilder extends TCPFile {
        protected Socket s;
        protected InetSocketAddress isA;
        protected ServerSocket ss;
        protected String msOut;
        protected OutputStream out;
        protected InputStream in = null;
        final int timer = 1000;
    TCPClientBuilder() {
            s = null;
            isA = null;
        }
        protected void setSocket() throws IOException {
            isA = new InetSocketAddress("localhost",8085);
            s = new Socket(isA.getHostName(), isA.getPort());
            s.setSoTimeout(5000);
            setStreamBuffer(s.getReceiveBufferSize());
/** we can include more setting, later ... */
        }
        protected void endSocket() throws SocketException {
            s.setSoTimeout(5000);
            setStreamBuffer(s.getReceiveBufferSize());
        }
}
