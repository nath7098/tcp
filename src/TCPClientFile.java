import java.io.InputStream;

public class TCPClientFile extends TCPClientBuilder implements Runnable {
    @Override
    public void run() {
        try {
            setSocket();
            System.out.println("TCPClientFile launched ...");
            in = s.getInputStream();
            write(in, "test.xml");
            System.out.println("TCPClientFile file received ...");
            in.close();
            s.close();
        } catch (Exception var2) {
            System.out.println("Exception TCPClientFile");
        }
    }
}


