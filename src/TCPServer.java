import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer extends TCPInfo implements Runnable {

   protected ServerSocket ss; protected Socket s;
   protected InetSocketAddress isA;
// the passive and active sockets
// the address
    /** The main method for threading. */
    public TCPServer() {
        ss = null; s = null;
        isA = new InetSocketAddress("localhost",8085);
    }
    /** The main method for threading. */
    public void run( ) {
        try {
            System.out.println("TCPServer launched ...");
            ss = new ServerSocket(isA.getPort());
            s = ss.accept();
            System.out.println("Hello, the server accepts");
            s.close();
            ss.close();
        } catch (Exception var2) {
            System.out.println("Exception TCPServer");
        }
    }

}



