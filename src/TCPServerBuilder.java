import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerBuilder extends TCPMessage {
    protected Socket s;
    protected InetSocketAddress isA;
    protected ServerSocket ss;
    protected InputStream in = null;
    final int timer1 = 10000;
    final int timer2 = 1000;
    protected int count;
    //protected byte[] buffer = new byte[8192];
    protected final int size = 8192;
    protected String msgIn;
    /** The set method for the buffer. */
    void setStreamBuffer(int size) {
        if(size>0)
            buffer = new byte[size];
        else
            buffer = new byte[size];
    }
    TCPServerBuilder() {
        ss = null; s = null;
        isA = new InetSocketAddress("localhost",8085);
    }
    protected void setSocket() throws IOException {
        isA = new InetSocketAddress("localhost",8085);
        ss = new ServerSocket(isA.getPort());
        setStreamBuffer(ss.getReceiveBufferSize());


/** we can include more setting, later ... */
    }
}
