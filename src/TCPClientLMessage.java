public class TCPClientLMessage extends TCPClientBuilder implements Runnable {
    @Override
    public void run() {
        try {
            setSocket();
            out = s.getOutputStream();
            loopWriteMessage(out, 512);
            out.close();
            s.close();
        } catch (Exception var2) {
            System.out.println("Exception TCPClientLMessage");
        }

    }
}
