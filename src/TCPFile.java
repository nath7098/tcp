import java.io.*;

public class TCPFile extends TCPMessage {
    private int count;
    private FileInputStream in;
    private StringBuffer msIn = new StringBuffer();
    private FileOutputStream out;

    TCPFile() {
    }

    void transfer(OutputStream out, String s) throws IOException {
        in = new FileInputStream(s);

        do {
            this.count = in.read(buffer);
            if (this.count > 0) {
                out.write(buffer, 0, count);
                out.flush();
            }
        } while(this.count > 0);

        this.in.close();
    }

    void print(InputStream in) throws IOException {
        count = 0;

        do {
            count = in.read(buffer);
            if (count > 0) {
                for(int i = 0; i < count; ++i) {
                    msIn.append((char)buffer[i]);
                }

                System.out.println(msIn);
                msIn.delete(0, msIn.length());
            }
        } while(count > 0);

    }

    void write(InputStream in, String s) throws IOException {
        out = new FileOutputStream(s);
        count = 0;

        do {
            count = in.read(buffer);
            if (count > 0) {
                out.write(buffer, 0, count);
            }
        } while(count > 0);

        out.close();
    }
}
