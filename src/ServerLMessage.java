import java.net.Socket;

public class ServerLMessage extends TCPClientBuilder implements Runnable {
    ServerLMessage(Socket s) {
        this.s = s;
    }

    public void run() {
        try {
            endSocket();
            System.out.println("ServerLMsg receiving ...");
            in = s.getInputStream();
            loopReadMessage(in);
            in.close();
            s.close();
        } catch (Exception var2) {
            System.out.println("Exception ServerLMsg");
        }

    }
}
