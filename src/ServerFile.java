import java.net.Socket;

public class ServerFile extends TCPClientFile implements Runnable{
    private static final String dbName = "db-small.xml";

    public ServerFile(Socket s) {
        this.s = s;
    }

    public void run() {
        try {
            endSocket();
            System.out.println("ServerFile transfering ...");
            out = s.getOutputStream();
            transfer(out, dbName);
            out.close();
            s.close();
            System.out.println("ServerFile transfer done ...");
        } catch (Exception var2) {
            System.out.println("Exception ServerFile");
        }

    }
}
