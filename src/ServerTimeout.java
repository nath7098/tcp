import java.io.IOException;
import java.net.Socket;

public class ServerTimeout extends TCPClientBuilder implements Runnable {
    String msIn;
    ServerTimeout(Socket s) {this.s = s;}
    public void run() {
        try {
            endSocket();
            sInfo("s2", s);
            in = s.getInputStream();
            msIn = readMessage(in);
            in.close();
            s.close();
        } catch (Exception var2) {
            System.out.println("Exception ServerTimeout");
        }
    }
}
